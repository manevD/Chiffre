import java.io.Console;

public class PlayfairChiffre  extends ChiffreText {

    private String map;

    public  PlayfairChiffre(String schluessel)
    {
        super(schluessel);
        String collection = "abcdefghijklmnopqrstuvwxyz";
        map = "";
        addChars(schluessel);
        addChars(collection);
    }

    private void addChars(String s)
    {
        for (char c: s.toLowerCase().toCharArray()){
            if (c== 'j') continue;
            if (c - 'a' < 0 || c - 'a' >25)continue;
            if (!map.contains(String.valueOf(c))){
                map +=c;
            }
        }
    }

    private int[] getPos(char c){
        for (int i = 0; i <map.length();i++){
          if (map.charAt(i)== c){
              return  new int[]{i%5, i/5};
          }
        }
        return  null;
    }

    private char getChar(int[] pos){
        int index = ((pos[0]+5)%5)+(((pos[1]+5)/5)*5);
        return  map.charAt(index);
    }

    private String encryptPair(String s, boolean reverse){
        int[] pos1 = getPos(s.charAt(0));
        int[] pos2 = getPos(s.charAt(1));
        int dir  = reverse ? -1:1;

        if (pos1[1] == pos2[1]) {
            pos1[0] = pos1[0] + dir;
            pos2[0] = pos2[0] + dir;
        } else if (pos1[0] == pos2[0]) {
            pos1[1] = pos1[1] + dir;
            pos2[1] = pos2[1] + dir;
        } else {
            int temp = pos1[0];
            pos1[0] = pos2[0];
            pos2[0] = temp;
        }
        return String.valueOf(getChar(pos1)) + String.valueOf(getChar(pos2));
    }
    private String[] splitPairs(String s) {
        String p = "";
        for (char c : s.toLowerCase().toCharArray()) {
            if (c - 'a' < 0 || c - 'a' > 25) continue;
            char i = c == 'j' ? 'i' : c;
            if (p.length() > 0 && p.charAt(p.length()-1)==i)
                p += 'x';
            p += i;
        }
        if (p.length() % 2 == 1) {
            p += 'x';
        }
        String[] pairs = new String[p.length() / 2];
        for (int i = 0; i < p.length(); i += 2) {
            String pair = "";
            pair += p.charAt(i);
            pair += p.charAt(i + 1);
            pairs[i / 2] = pair;
        }
        return pairs;
    }

    @Override
    public void doEncrypt() {
        String[] pairs = splitPairs(klarText);
        String res = "";
        for (String p : pairs) {
            res += encryptPair(p,true);
        }
        crypttext = res;
    }

    @Override
    public void doDecrypt() {
        String[] pairs = splitPairs(crypttext);
        String res = "";
        for (String p : pairs) {
            res += encryptPair(p, true);
        }
        klarText = res;
    }
}
