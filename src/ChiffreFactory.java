
public class ChiffreFactory {

    public static ChiffreText getChiffre(ChiffreEnum e,String s){
        switch (e){
            case CEASAR -> new CaesarChiffreText(s);
            case NIX -> new NixChiffreText(s);
            case SUBST -> new CaesarChiffreText(s);
            case PLAYFAIR -> new PlayfairChiffre(s);
            case POLYBIOS -> new PolybiosChiffre(s);
        }
        return null;
    }
}
