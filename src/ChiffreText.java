
public abstract class ChiffreText {
    protected String schluessel;
    protected String klarText;
    protected String crypttext;

    public ChiffreText(String schluessel){
        setSchluessel(schluessel);
    }

    public void setCrypttext(String crypttext){
        this.crypttext = crypttext;
    }
    public String getCrypttext(){
        return crypttext;
    }

    public void setKlarText(String klarText) {
        this.klarText = klarText;
    }
    public String getKlarText(){
        return klarText;
    }

    public void setSchluessel(String schluessel)
    {
        this.schluessel = schluessel;
    }
    public String getSchluessel(){
        return schluessel;
    }

    public abstract void doEncrypt();
    public abstract void doDecrypt();
}
