
public class NixChiffreText extends ChiffreText {

    public NixChiffreText(String schluessel){
        super(schluessel);
    }

    @Override
    public void doEncrypt() {
        this.setCrypttext(this.getKlarText());
    }

    @Override
    public void doDecrypt() {
        this.setKlarText(this.getCrypttext());
    }
}
