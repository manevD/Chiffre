
public class PolybiosChiffre extends ChiffreText {
    private String map;
    public PolybiosChiffre(String schluessel){
        super(schluessel);
        String collection = "abcdefghijklmnopqrstuvwxyz ";
        map = "";
        addChars(schluessel);
        addChars(collection);
    }

    private void addChars(String s) {
        for (char c : s.toLowerCase().toCharArray()) {
            if (!map.contains(String.valueOf(c))) {
                map += c;
            }
        }
    }
    private char getChar(String code) {
        int a = code.charAt(0) - '0';
        int b = code.charAt(1) - '0';
        int index = (a - 1) * 5 + (b - 1);
        return map.charAt(index);
    }

    @Override
    public void doEncrypt() {
        String out = "";
        for (char c : klarText.toLowerCase().toCharArray()) {
            out += getCode(c) + " ";
        }
        crypttext = out;
    }

    private String getCode(char c) {
        for (int i = 0; i < map.length(); i++) {
            if (map.charAt(i) == c) {
                return (i / 5 + 1) + "" + (i % 5 + 1);
            }
        }
        return null;
    }

    @Override
    public void doDecrypt() {
        String out = "";
        char[] codes = crypttext.replace(" ", "").toCharArray();
        for (int i = 0; i < codes.length; i += 2) {
            String code = String.valueOf(codes[i]) + String.valueOf(codes[i+1]);
            out += getChar(code);
        }
        klarText = out;
    }
}
