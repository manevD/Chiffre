public class CaesarChiffreText extends ChiffreText {
    private int offset;

    public CaesarChiffreText(String schluessel)
    {
        super(schluessel);
        calcOffset(schluessel);
    }

    @Override
    public void doEncrypt() {
        String out = "";
        for (char c : this.getKlarText().toLowerCase().toCharArray()) {
            int i = (c - 'a' + offset) % 26;

            out += (char) (i + 'a');
        }
        this.setCrypttext(out);
    }

    @Override
    public void doDecrypt() {
        String out = "";
        for (char c : this.getCrypttext().toLowerCase().toCharArray()) {
            int i = ((c - 'a') - offset + 26) % 26;

            out += (char) (i + 'a');
        }
        this.setKlarText(out);
    }
    public void calcOffset(String schluessel) {
        this.offset = schluessel.toLowerCase().charAt(0) - 'a';
    }
}
