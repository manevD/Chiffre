package Stack;

import java.util.ArrayList;

public class DerStack {

    private static ArrayList<Character> derStackYo = new ArrayList<>();
    public static  void push(Character x){
        derStackYo.add(x);
    }

    public static Character pop(){
        int size = derStackYo.size()-1;
        char getChar = derStackYo.get(size);
        derStackYo.remove(size);
        return  getChar;
    }

    public static int topOfStack(){
        return  derStackYo.size();
    }
}
